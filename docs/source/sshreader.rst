sshreader API
=============


.. automodule:: sshreader.utils
   :members: Hook, ServerJob, sshread, shell_command, echo


Indices and tables
------------------

* :ref:`sshreader`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* `JA Computing`_

.. _JA Computing: http://www.jacomputing.net